import java.io.*;
import java.util.*;
import javax.swing.*;

public class WordCounter {
	String wc = null; //string for word count output.
	String key = null; //string for unique word instances output.
    String uw = null; //string for unique word count.
 
   public void count(File file, JTextArea textarea) {
	   HashMap<String, Integer> map = new HashMap<String, Integer>(); //Hashmap stores unique words.
       BufferedReader reader = null; //bufferedreader to read file
        
       //Initializing wordCount to 0
       int mywordcount =0;
        
       try
       {
           //Creating BufferedReader object
            
           reader = new BufferedReader(new FileReader(file));
            
           //Reading the first line into currentLine

           String currentLine = reader.readLine();
            
           while (currentLine != null)
           {
        	   
                
               //Getting number of words in currentLine by splitting line into strings"words" when encountering spaces. Also replaces ponctuation and numbers with """", to be removed later.
                
               String[] words = currentLine.replaceAll("^[,\\s\\W\\d]+", "").split("[,\\s]+");
   
               
               //Reading next line into currentLine. //Iterating each word.
               for( int i = 0; i <words.length; i++)
               {
               String word = words[i].replaceAll("[\\W\\d]", "").toLowerCase(); //replaces ponctuation apart of words and changes all words into lowercase for comparison later.

               if(map.containsKey(word)) //map contains words
               {
               map.put(word, map.get(word) + 1); //increments the instances a unique word appears //lowercase allows word to be count as same word rather uppercase or not.
               }
               else 
               {
               map.put(word, 1); //else just set instances of unique word to 1
               map.remove(""); //removes the replaced ponctuation and numbers so its not counted in word count.
           }
               }
            
               currentLine = reader.readLine(); //moves loop to next line.
           }
           
           for (Map.Entry<String, Integer> entry : map.entrySet()) //finds entry in map
           {
               String key = ( "\n" + entry.getKey() + ": x" + entry.getValue()+" "); //prints unique word, and instances.
               textarea.setText(textarea.getText() +key); //prints output above to textarea.
        	   mywordcount = mywordcount+entry.getValue(); // adds to total number of words.
       }
           String wc = ("\n"+"Word Count: "+mywordcount); //puts wordcount total into string.
           String uw = ("\n" + "Unique Words: "+map.size()); //puts unique word count into string.
           textarea.setText(textarea.getText()+wc); //prints wordcount total into textarea.
           textarea.setText(textarea.getText()+uw); //prints unique wordcount total into textarea.
           textarea.setLineWrap(true);
           textarea.setVisible(true);
       }
       catch (IOException e) 
       {
           e.printStackTrace();
       }
       finally
       {
           try
           {
               reader.close();           //Closing the reader
           }
           catch (IOException e) 
           {
               e.printStackTrace();
           }
       }
   }
       }
