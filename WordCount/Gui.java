import java.awt.event.ActionEvent; //import Action event library
import java.awt.event.ActionListener; //import Action listener Library
import java.io.*;
import javax.swing.*; //import javax swing library

public class Gui extends JFrame implements ActionListener
{ //Gui Class extends as frame that implements the button listener/response.
			  JFileChooser bloop = new JFileChooser(); //Establishes JfileChooser class.
			  JButton selectionbutton = new JButton("Select"); //Jbutton creating button for selecting file.
			  JButton countbutton = new JButton("Count"); //Jbutton creating button for activating count functionality.
			  JTextArea textarea = new JTextArea(); //Creates TextArea for output.
			  WordCounter wordcount = new WordCounter(); //Establishes our wordcounter class inside this class.
			  JScrollPane scroll = new JScrollPane(textarea);//Creates a scroller for textarea.

		   public Gui() { //Frame
			  setTitle("                                Word Counter Application"); //Title
			  setLayout(null); //allows components inside frame to move around.
			  setVisible(true); //makes frame and components visible.
			  add(scroll); //adds scroll to frame.
			  scroll.setBounds(21,142,470,85); //sets size for scroll/textarea.
			  setLocationRelativeTo(null);
		      setSize(525, 275); //size of frame.
		      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //ends code on exit of frame.
		      add(selectionbutton); //adds select button.
		      add(countbutton); //adds count button.
		      setResizable(false); //disables sizing frame.
		      countbutton.setBounds(211,95,90,30); //button size.
		      countbutton.addActionListener(this); //Listener for count button upon click will execute code in actionlistener.
		      selectionbutton.setBounds(211,40,90,30); //button size
		      selectionbutton.addActionListener(this); //listens for users click on button to trigger code in actionlistener.
		      scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS); //Vertical scroll
		      textarea.setEditable(false);//disables user editing inside text area 
		   }
		public void actionPerformed(ActionEvent p) { //listens for action.
			if(p.getSource() == selectionbutton){ //when select button is clicked.
			int returnVal = bloop.showOpenDialog(null); //Opens dialog box that allows user to choose file.
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				File file = bloop.getSelectedFile();
				JOptionPane.showMessageDialog(selectionbutton, //pops small window tellign user what file was chosen.
						bloop.getSelectedFile().getName()+ "\n" + "(" + file + ")",
					    "Chosen File",
					    JOptionPane.PLAIN_MESSAGE);
			}
			}
					
			if(p.getSource() == countbutton){ // when count button is selected.
				File file = bloop.getSelectedFile(); //gets file from filechooser.
				wordcount.count(file, textarea); //inputs file and textarea into count() parameters.
			}
		}
		   
}
