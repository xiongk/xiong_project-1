# README #

##Contents##
What is this repository for?
Version


### What is this repository for? ###

* 
This project will build, using Java, an application which can open up a text document, count the total number of words, the total number of different words (ignoring case) and the count for each different word. It should properly configure and use a java.util.Scanner object to scan through an input file and pull out the words while ignoring all the punctuation. Using java.util.Hashmap object to track the words found. Finally a user interface in swing or javafx so that Users can run the app, open a file with a file chooser, and have the counts of the words displayed within the application or written out to a file, or both.

For this particular project, it will be developed and tested with Java Eclipse on a Windows 10 Intel Core i5 PC.

* Version 1.0.